import UIKit
import GLKit
import app

class ViewController: GLKViewController, GLKViewControllerDelegate, Paprika_canvasCanvas {
    
    var height: Int32 = 0
    
    var width: Int32 = 0
    
    var height_: Int32 = 0
    var pixelSize: Paprika_komlSizef = Paprika_komlSizef(width: 1,height: 1)
    var width_: Int32 = 0
    var paprika: Paprika_centralPaprika? = nil
    var flappyPlane:FlappyPlaneApp? = nil
    
    private var context: EAGLContext?
    private var updater: ((KotlinFloat) -> Void)?
    
    private func setupGL() {
//        if let imagePath = Bundle.main.path(forResource: "background.png", ofType: nil)
//        {
//            UIImage(contentsOfFile: imagePath)
//        }

        
//        paprika = Paprika_backendIOSPaprika(self)
        // 1
        print(EAGLRenderingAPI.openGLES1.rawValue)
        
        print(EAGLRenderingAPI.openGLES2.rawValue)
        
        print(EAGLRenderingAPI.openGLES3.rawValue)
        context = EAGLContext(api: .openGLES2)
        // 2
        EAGLContext.setCurrent(context)
        
        flappyPlane = FlappyPlaneApp()
        flappyPlane!.launch(canvas: self)
        
        if let view = self.view as? GLKView, let context = context {
            // 3
            view.context = context
            // 4
            delegate = self
        }
    }
    
    func update(action: @escaping (KotlinFloat) -> Void) {
        updater = action
    }
    
    func glkViewControllerUpdate(_ controller: GLKViewController) {
    
        updater?(KotlinFloat(float:1/60 ))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let v  = self.view as! GLKView
        self.height = Int32(v.drawableHeight)
        self.width = Int32(v.drawableWidth)
    }
    
    override func viewDidLoad()  {
        super.viewDidLoad()
//        let value = UIInterfaceOrientation.landscapeLeft.rawValue
//        UIDevice.current.setValue(value, forKey: "orientation")
       
        setupGL()
    }
    
    override func glkView(_ view: GLKView, drawIn rect: CGRect) {
        // 1
//        glClearColor(0.85, 0.85, 0.85, 1.0)
        // 2
//        glClear(GLbitfield(GL_COLOR_BUFFER_BIT))
    }
    
}
