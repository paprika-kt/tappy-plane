import UIKit
import GLKit
import app

class ViewController: GLKViewController, GLKViewControllerDelegate, Paprika_canvasCanvas {
    
    var height: Int32 = 0
    
    var width: Int32 = 0
    
    var height_: Int32 = 0
    var pixelSize: Paprika_komlSizef = Paprika_komlSizef(width: Float(UIScreen.main.scale),height: Float(UIScreen.main.scale))
    var width_: Int32 = 0
    var paprika: Paprika_centralPaprika? = nil
    var flappyPlane:FlappyPlaneApp? = nil
    var lastTime = CACurrentMediaTime()
    
    private var context: EAGLContext?
    private var updater: ((KotlinFloat) -> Void)?
    
    private func setupGL() {
        // 1
        print(EAGLRenderingAPI.openGLES1.rawValue)
        
        print(EAGLRenderingAPI.openGLES2.rawValue)
        
        print(EAGLRenderingAPI.openGLES3.rawValue)
        context = EAGLContext(api: .openGLES2)
        // 2
        EAGLContext.setCurrent(context)
        
        flappyPlane = FlappyPlaneApp()
        flappyPlane!.launch(canvas: self)
        
        if let view = self.view as? GLKView, let context = context {
            // 3
            view.context = context
            // 4
            delegate = self
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        let touch = touches.first!
        let location = touch.location(in: self.view)
        flappyPlane?.paprika.mouse.trigger(touch: touch,x: Int32(Float(location.x)*pixelSize.width), y: Int32(Float(location.y)*pixelSize.height))
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?){
        let touch = touches.first!
        let location = touch.location(in: self.view)
        flappyPlane?.paprika.mouse.trigger(touch: touch,x: Int32(Float(location.x)*pixelSize.width), y: Int32(Float(location.y)*pixelSize.height))
    }
    
    func update(action: @escaping (KotlinFloat) -> Void) {
        updater = action
    }
    
    func glkViewControllerUpdate(_ controller: GLKViewController) {
    
        let time = CACurrentMediaTime()
        updater?(KotlinFloat(float:Float(time-lastTime)))
        lastTime = time
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let v  = self.view as! GLKView
        self.height = Int32(v.drawableHeight)
        self.width = Int32(v.drawableWidth)
    }
    
    override func viewDidLoad()  {
        super.viewDidLoad()
        setupGL()
    }
    
    override func glkView(_ view: GLKView, drawIn rect: CGRect) {

    }
    
}
