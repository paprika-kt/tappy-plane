package paprika.tappyPlane

import paprika.koml.geom.MutableMatrix3f
import paprika.koml.geom.vectorOf
import paprika.tappyPlane.components.Button
import paprika.tappyPlane.utils.Assets
import paprika.Paprika
import paprika.renderer.Renderer2D
import paprika.renderer.graphics.Camera2D
import paprika.renderer.graphics.text.draw
import paprika.renderer.graphics.text.textWidth

class HomeScreen(val paprika: Paprika, camera: Camera2D, private val renderer2D: Renderer2D, val assets: Assets) {

    val font = assets.font
    val w = (paprika.canvas.height * 800) / 480

    val center = vectorOf(800 / 2f, 480 / 2f)
    val titleWith = font.textWidth("TAPPY PLANE")
    val playButton = Button(paprika.mouse, font, "PLAY", assets["buttonSmall"], camera, 10).apply {
        x = center.x - width / 2f
        y = center.y
    }


    fun update(): Boolean {
        renderer2D.draw(font, "TAPPY PLANE", MutableMatrix3f()
                .translate(center.x - titleWith / 2f, center.y - 100))
        return playButton.update(renderer2D)
    }

}
