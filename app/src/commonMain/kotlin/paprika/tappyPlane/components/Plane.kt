package paprika.tappyPlane.components

import paprika.koml.geom.MutableMatrix3f
import paprika.koml.geom.RectangleF
import paprika.tappyPlane.helper.AnimationUv
import paprika.tappyPlane.utils.Assets
import paprika.input.Mouse
import paprika.renderer.Renderer2D

class Plane(val mouse: Mouse, renderer2D: Renderer2D, assets: Assets, var fps: Float = 3f) {
    private val frames = arrayListOf(assets["planeRed1"], assets["planeRed3"], assets["planeRed2"])
    private val animationUv = AnimationUv(renderer2D, frames, fps)
    val height = frames[0].height

    private var started: Boolean = false

    private val gravity = 300f
    private var force = 0f
    val rectangle: RectangleF
        get() = RectangleF(animationUv.vertices[0], animationUv.vertices[1], frames[0].width, frames[0].height)

    fun start() {
        started = true
    }

    fun update(delta: Float) {
        animationUv.update(delta)
        if (started) {
            if (mouse.hasButtonDown(0))
                up()
            force += gravity * delta
            animationUv.transform(MutableMatrix3f().translate(0f, force * delta))
        }
    }

    fun up() {
        force = -200f
    }

    fun transform(matrix3f: MutableMatrix3f) {
        animationUv.transform(matrix3f)
    }

    fun stop() {
        started = false
    }
}