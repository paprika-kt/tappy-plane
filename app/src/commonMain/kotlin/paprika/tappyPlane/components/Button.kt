package paprika.tappyPlane.components

import paprika.koml.geom.MutableMatrix3f
import paprika.koml.geom.vectorOf
import paprika.input.Mouse
import paprika.renderer.Renderer2D
import paprika.renderer.graphics.Camera2D
import paprika.renderer.graphics.text.font.Font
import paprika.renderer.graphics.text.draw
import paprika.renderer.graphics.text.textWidth
import paprika.renderer.graphics.sprite.*

class Button(
    private val mouse: Mouse,
    private val font: Font,
    private val text: String,
    private val button: SlicedSprite,
    private val camera: Camera2D
) : Sprite by button {

    private val textWidth = font.textWidth(text)
    private val margin: Float = 10f

    private val pointer = mouse.pointer

    private var over: Boolean = false
    private var down: Boolean = false

    private val fontMatrix3f = MutableMatrix3f().translate(margin, 10f)

    var x: Float
        get() = button.x
        set(value) {
            fontMatrix3f.translate(value - button.x, 0f)
            button.x = value
        }

    var y: Float
        get() = button.y
        set(value) {
            fontMatrix3f.translate(0f, value - button.y)
            button.y = value
        }

    var width: Float = 0f

    init {
        button.setColor(0.95f, 0.95f, 0.95f)
        button.width = textWidth + margin * 2
        width = button.width
    }

    constructor(mouse: Mouse, font: Font, text: String, quad: Quad, camera: Camera2D, marge: Int) : this(
        mouse,
        font,
        text,
        quad.quadToSlicedSprite(marge, marge, marge, marge),
        camera
    )

    fun update(renderer2D: Renderer2D): Boolean {
        val m = camera.unProject(vectorOf(pointer.x.toFloat(), pointer.y.toFloat(), 0f))

        val mouseOver = button.rectangle.contains(m.x, m.y)

        if (mouseOver && !over) {
            over = true
            button.setColor(1f, 1f, 1f)
        } else if (!mouseOver && over) {
            over = false
            button.setColor(0.95f, 0.95f, 0.95f)
        }
        if (mouse.hasButtonDown(0) && over && !down) {
            down = true
            button.transform(MutableMatrix3f().translate(0f, 5f))
//            button.crop(height = button.height - 5)
        } else if (mouse.hasButtonDown(0) && !over && down) {
            down = false
            button.transform(MutableMatrix3f().translate(0f, -5f))
//            button.crop(height = button.height + 5)
        } else if (!mouse.hasButtonDown(0) && over && down) {
            down = false
            button.transform(MutableMatrix3f().translate(0f, -5f))
//            button.crop(height = button.height + 5)
            return true
        }
        renderer2D.draw(button)

        if (down)
            renderer2D.draw(font, text, fontMatrix3f)
        else if (!down)
            renderer2D.draw(font, text, fontMatrix3f.copy(MutableMatrix3f()).translate(0f, -5f))
        return false
    }
}
