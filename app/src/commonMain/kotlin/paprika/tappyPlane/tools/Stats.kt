package paprika.tappyPlane.tools

import paprika.koml.geom.MutableMatrix3f
import paprika.renderer.Renderer2D
import paprika.renderer.graphics.text.font.Font
import paprika.renderer.graphics.text.draw
import kotlin.math.roundToInt


class Stats(private val renderer2D: Renderer2D, val font: Font) {

    private var _totalDelta = 0f
    private var _counter = 0
    private var _delay = 1
    private var _fps = -1f
    public val matrix = MutableMatrix3f()


    fun render(delta: Float) {
        val m = matrix.copy(MutableMatrix3f())
        _totalDelta += delta
        _counter++
        if (_fps > 0)
            renderer2D.draw(font, "fps ${(_fps).roundToInt()}", m)

        if (_totalDelta > _delay) {
            _fps = _counter / (_totalDelta)
            _counter = 0
            _totalDelta = 0f
        }
        renderer2D.flush()
        renderer2D.flush()
    }
}