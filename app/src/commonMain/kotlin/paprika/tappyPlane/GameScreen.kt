package paprika.tappyPlane

import ca.jsbr.logkt.context
import ca.jsbr.logkt.klog
import paprika.koml.geom.MutableMatrix3f
import paprika.koml.geom.TriangleF
import paprika.koml.geom.vectorOf
import paprika.tappyPlane.components.Plane
import paprika.tappyPlane.utils.Assets
import paprika.Paprika
import paprika.renderer.Renderer2D
import paprika.renderer.graphics.text.draw
import paprika.renderer.graphics.sprite.Quad


class GameScreen(paprika: Paprika, private val renderer2D: Renderer2D, assets: Assets) {

    private val logger by klog.context()
    private val font = assets.font

    private val screenWidth = 800
    private val screenHeight = 480

    private val speed = 80f
    private val center = vectorOf(screenWidth / 2f, screenHeight / 2f)
    private var distance = 0f
    private var started = false
    private var _gameOver = false

    private val backgroundParallaxItem = ScrollItem(renderer2D, speed * 0.5f, assets.background, 3)
    private var ground = assets["groundGrass"].clone().apply {
        y = screenHeight.toFloat() - height
    }.toScrollItem(renderer2D, speed * 0.95f, 2)
    private var sky = assets["groundGrass"].let {
        Quad(it.texture, it.x, it.y, it.width, it.height, it.u2, it.v2, it.u, it.v)
    }.toScrollItem(renderer2D, speed * 0.95f, 2)


    private val rocksTop = assets["rockGrassDown"].clone().apply {
        x = 600f
    }.toScrollItem(renderer2D, speed, 3, 600f)
    private val rocksDown = assets["rockGrass"].clone().apply {
        x = 350f
        y = screenHeight - height
    }.toScrollItem(renderer2D, speed, 3, 600f)

    private val plane = Plane(paprika.mouse, renderer2D, assets, 24f).apply {
        transform(MutableMatrix3f().translate(60f, center.y - height / 2))
    }

    fun start() {
        backgroundParallaxItem.start()
        ground.start()
        sky.start()
        rocksTop.start()
        rocksDown.start()
        plane.start()
        started = true
    }

    private fun stop() {
        backgroundParallaxItem.stop()
        ground.stop()
        sky.stop()
        rocksTop.stop()
        rocksDown.stop()
        plane.stop()
        started = false
    }

    private fun gameOver() {
        stop()
        _gameOver = true
    }

    fun update(delta: Float): Boolean {
        val planeRect = plane.rectangle.expend(-2f, -4f)
        backgroundParallaxItem.update(delta)
        ground.update(delta)
        sky.update(delta)
        plane.update(delta)
        sky.items.forEach {
            if (it.rectangle.intersect(planeRect))
                gameOver()
        }
        ground.items.forEach {
            if (it.rectangle.intersect(planeRect))
                gameOver()
        }

        rocksTop.update(delta)
        rocksDown.update(delta)

//        renderer2D.draw(rocksDown)

        rocksTop.items.forEach {
            if (it.triangleReverse.intersects(planeRect))
                gameOver()
        }
        rocksDown.items.forEach {
            if (it.triangle.intersects(planeRect))
                gameOver()
        }

        if (started)
            distance += delta * speed
        if (started || _gameOver)
            renderer2D.draw(font, distance.toInt().toString(), MutableMatrix3f().scale(0.6f, 0.6f).translate(10f, 10f))


        return _gameOver
    }
}

private val Quad.triangle
    get() = TriangleF(vectorOf(x, y + height), vectorOf(x + width, y + height), vectorOf((x + width / 2f) + 12f, y))


private val Quad.triangleReverse
    get() = TriangleF(vectorOf(x, y), vectorOf(x + width, y), vectorOf((x + width / 2f) + 12f, y + height))

private fun Quad.toScrollItem(renderer2D: Renderer2D, speed: Float, count: Int, width: Float = this.width): ScrollItem {
    return ScrollItem(renderer2D, speed, this, count, width)
}

