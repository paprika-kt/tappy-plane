package paprika.tappyPlane

import paprika.renderer.graphics.sprite.Quad

class PoolManager(val sprite: Quad, val max: Int = 10) {
    val pools = Array(max) { sprite.clone() }
    var current: Int = 0
    fun next(): Quad {
        if (current >= max)
            current = 0
        val item = pools[current++]
        sprite.vertices.forEachIndexed { index, fl -> item.vertices[index] = fl }
        return item
    }
}