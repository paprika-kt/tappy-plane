package paprika.tappyPlane

import paprika.renderer.Renderer2D
import paprika.renderer.graphics.sprite.Quad

class ScrollItem(
        private val renderer2D: Renderer2D,
        private val speed: Float,
        val quad: Quad,
        private val count: Int,
        private val width: Float = quad.width) {

    val items = Array(count) {
        val q = quad.clone()
        q.x += it * width
        q
    }
    private var started: Boolean = false

    fun start() {
        started = true
    }

    fun stop() {
        started = false
    }

    fun update(delta: Float) {
        items.forEach {
            if (started) {
                it.x += -speed * delta
                if (it.x + width <= 0)
                    it.x += width * count
            }
            renderer2D.draw(it)
        }
    }
}