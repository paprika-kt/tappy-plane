package paprika.tappyPlane

import paprika.koml.geom.MutableMatrix3f
import paprika.koml.geom.vectorOf
import paprika.tappyPlane.components.Button
import paprika.tappyPlane.utils.Assets
import paprika.Paprika
import paprika.renderer.Renderer2D
import paprika.renderer.graphics.Camera2D
import paprika.renderer.graphics.sprite.transform


class EndScreen(val paprika: Paprika, val camera: Camera2D, private val renderer2D: Renderer2D, assets: Assets) {

    private val font = assets.font
    private val center = vectorOf(800 / 2f, 480 / 2f)
    private val gameOver = assets["textGameOver"].clone().apply {
        transform(MutableMatrix3f().translate(center.x - width / 2f, center.y - 100))
    }
    val retryButton = Button(paprika.mouse, font, "RETRY", assets["buttonSmall"], camera, 10).apply {
        x = center.x - width / 2
        y = center.y
    }

    fun update(): Boolean {
        renderer2D.draw(gameOver)
        return retryButton.update(renderer2D)
    }
}