package paprika.tappyPlane.helper

import paprika.koml.geom.MutableMatrix3f
import paprika.koml.geom.transform2d
import paprika.renderer.Renderer2D
import paprika.renderer.graphics.sprite.Quad
import paprika.renderer.graphics.texture.Texture


class AnimationUv(val renderer2D: Renderer2D, val refSprites: ArrayList<Quad>, val fps: Float = 15f) {
    val texture: Texture = refSprites[0].texture
    val vertices: FloatArray = refSprites[0].vertices.copyOf()
    val textCoords = refSprites.map { it.textCoord.copyOf() }
    val indices = refSprites[0].indices.copyOf()
    val colors: FloatArray = refSprites[0].colors.copyOf()

    private var time = 0f

    fun transform(matrix4f: MutableMatrix3f) {
        matrix4f.transform2d(vertices)
    }

    fun update(delta: Float) {
        time += delta * fps
        renderer2D.draw(texture, vertices, textCoords[time.toInt() % textCoords.size], indices, colors)
    }

}