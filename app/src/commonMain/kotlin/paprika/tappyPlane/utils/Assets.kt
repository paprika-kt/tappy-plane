package paprika.tappyPlane.utils

import paprika.renderer.graphics.text.toFont
import paprika.renderer.graphics.sprite.Quad
import paprika.renderer.graphics.texture.Texture

class Assets(
    private val atlas: Map<String, Quad>,
    background: Texture
) {
    operator fun get(key: String): Quad = atlas[key] ?: error("$key not found in atlas")
    val letters: Map<String, Quad> = atlas.filter { it.key.length == 1 }
    val font = letters.mapKeys { it.key[0] }.toFont()
    val background = Quad(background)
}