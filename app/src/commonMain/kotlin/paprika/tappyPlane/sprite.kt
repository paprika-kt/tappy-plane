package paprika.tappyPlane

import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.content
import kotlinx.serialization.json.int
import paprika.io.Path
import paprika.renderer.graphics.sprite.Quad
import paprika.renderer.graphics.texture.Texture


fun Texture.toSprites(data: JsonObject) = data["frames"]!!.jsonArray
        .map { it as JsonObject }
        .map {
            val frame = it["frame"] as JsonObject
            val name = Path.removeExtension(Path.baseName(it["filename"]!!.content))
            name to Quad.fromRegion(this, frame["x"]!!.int, frame["y"]!!.int, frame["w"]!!.int, frame["h"]!!.int)
        }
        .toMap()
