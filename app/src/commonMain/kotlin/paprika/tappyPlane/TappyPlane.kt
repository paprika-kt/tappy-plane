package paprika.tappyPlane

import ca.jsbr.logkt.context
import ca.jsbr.logkt.debug
import ca.jsbr.logkt.klog
import paprika.tappyPlane.tools.Stats
import paprika.tappyPlane.utils.Assets
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import paprika.Paprika
import paprika.io.task.Task
import paprika.io.task.all
import paprika.renderer.OrderedBatchingRenderer2D
import paprika.renderer.PGL
import paprika.renderer.graphics.Camera2D
import paprika.renderer.graphics.ScreenFill
import paprika.renderer.graphics.texture.toTexture


class TappyPlane(val paprika: Paprika) {

    private val logger by klog.context()

    private var assets: Assets? = null

    private var homeScreen: HomeScreen? = null
    private var gameScreen: GameScreen? = null
    private var endScreen: EndScreen? = null

    private val camera = Camera2D(paprika.kgl, 800, 480, ScreenFill.Contain)
    private var renderer2D = OrderedBatchingRenderer2D(paprika.kgl, camera.perspective)
    private val json = Json(JsonConfiguration.Stable)


    private var stats: Stats? = null

    fun init() {

        Task.all(
            paprika.assetStorage.readText("sprites.json"),
            paprika.assetStorage.readImage("sprites.png"),
            paprika.assetStorage.readImage("background.png")
        ).execute {
            val (data, image, bg) = it
            val atlas = image.toTexture(paprika.kgl).toSprites(json.parseJson(data).jsonObject)
            assets = Assets(atlas, bg.toTexture(paprika.kgl))
            onLoadFinish()
        }
    }

    private fun onLoadFinish() {
        logger.debug("onLoadFinish")
        homeScreen = HomeScreen(paprika, camera, renderer2D, assets!!)
        gameScreen = GameScreen(paprika, renderer2D, assets!!)
        stats = Stats(renderer2D, assets!!.font).apply {
            matrix.translate(0f, 430f).scale(0.3f,0.3f)
        }

        paprika.kgl.clearColor(0f, 0f, 0f, 0f)
        paprika.kgl.enable(PGL.DEPTH_TEST)
        paprika.canvas.update(this::update)
    }

    fun update(delta: Float) {
        paprika.kgl.clear(PGL.COLOR_BUFFER_BIT)
        camera.update(paprika.canvas.width, paprika.canvas.height)

        if (gameScreen?.update(delta) == true && endScreen == null) {
            endScreen = EndScreen(paprika, camera, renderer2D, assets!!)
        }
        if (homeScreen?.update() == true) {
            homeScreen = null
            gameScreen?.start()
        }
        if (endScreen?.update() == true) {
            endScreen = null
            gameScreen = GameScreen(paprika, renderer2D, assets!!)
            gameScreen!!.start()
        }
        stats?.render(delta)

        renderer2D.end()
    }

}
