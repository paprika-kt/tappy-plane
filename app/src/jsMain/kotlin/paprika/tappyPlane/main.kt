package paprika.tappyPlane

import paprika.KJSPaprika
import paprika.PaprikaOptions

fun main() {
    val paprika = KJSPaprika(PaprikaOptions.fromId("game", "assets/"))
    TappyPlane(paprika)
        .init()
}
