package paprika.tappyPlane

import android.util.Log
import paprika.tappyPlane.TappyPlane
import paprika.AndroidPaprika
import paprika.canvas.PaprikaActivity
import paprika.canvas.PaprikaGLSurfaceView

class TappyPlaneActivity : PaprikaActivity() {

    val paprika by lazy {
        AndroidPaprika(this)
    }
    override val glView: PaprikaGLSurfaceView
        get() = paprika.view

    override fun init() {
        Log.i("Paprika", "PaprikaSampleActivity")
        TappyPlane(paprika).init()
    }

}
