package ca.jsbr.tappyPlane

import paprika.LWJGLPaprika
import paprika.PaprikaOptions
import paprika.tappyPlane.TappyPlane
import java.io.Closeable
import java.io.File

fun main() {
    TappyPlane(LWJGLPaprika(PaprikaOptions(800, 480, "../assets/")))
            .init()
}