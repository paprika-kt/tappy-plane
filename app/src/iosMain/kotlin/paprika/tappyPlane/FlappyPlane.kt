package paprika.tappyPlane

import paprika.tappyPlane.TappyPlane
import paprika.IOSPaprika
import paprika.canvas.Canvas

class FlappyPlaneApp{
    lateinit var paprika: IOSPaprika

    fun launch(canvas: Canvas){
        paprika = IOSPaprika(canvas)
        TappyPlane(paprika)
            .init()
    }

    fun update(){
//        paprika.update()
    }
}