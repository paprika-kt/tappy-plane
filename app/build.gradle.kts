
plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.plugin.serialization") version "1.3.61"
    kotlin("multiplatform")
}
//
android {
    compileSdkVersion(28)
    defaultConfig {
        minSdkVersion(15)
        targetSdkVersion(28)
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "android.support.test.runner.AndroidJUnitRunner"
    }
    buildTypes {
    }

    lintOptions {
        tasks.lint.get().enabled = false
    }
    sourceSets["main"].assets.srcDirs("../assets", "src/main/assets")

    packagingOptions {
        exclude("META-INF/kotlinx-serialization-runtime.kotlin_module")
    }
}

version = "0.0.1"

repositories {
    jcenter()
    google()
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime:0.14.0")
}

kotlin {
    jvm()
    js {
        val main by compilations.getting {
            kotlinOptions {
                outputFile = "$projectDir/dist/js/${project.name}.js"
                sourceMap = true
                sourceMapEmbedSources = "always"
                noStdlib = false
            }
        }
    }
    iosX64("ios") {
        binaries {
            framework()
        }
    }

    android("android")

//    iosArm64("iosDevice") {
//        binaries {
//            framework()
//        }
//    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(kotlin("stdlib-common"))
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime-common:0.14.0")
                implementation(project(":paprika-game"))
                implementation(project(":klog"))
            }
        }
        val commonTest by getting {
            dependencies {
            }
        }

        val jvmMain by getting {
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime:0.14.0")

            }
        }

        val jsMain by getting {
            dependencies {
                implementation(kotlin("stdlib-js"))
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime-js:0.14.0")

            }
        }

        val jvmTest by getting {
            tasks.withType<Test> {
                useJUnitPlatform()
            }
            dependencies {
                implementation(kotlin("test"))
                implementation(kotlin("test-junit"))
                implementation("io.kotlintest:kotlintest-runner-junit5:3.4.2")
            }
        }

        val jsTest by getting {
            dependencies {
                implementation(kotlin("test-js"))
            }
        }


        //review really used?
        val androidMain by getting {
            dependencies {
                implementation(kotlin("stdlib"))
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime:0.14.0")
            }
        }

        val androidTest by getting {
            dependencies {
                implementation(kotlin("test"))
                implementation(kotlin("test-junit"))
            }
        }


        val iosMain by getting {
            dependencies {
                implementation(kotlin("stdlib"))
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime-native:0.14.0")
            }
        }

        val iosTest by getting {
            dependencies {
                implementation(kotlin("test"))
                implementation(kotlin("test-junit"))
            }
        }
    }
}

val compileTestJs = tasks.getByName("compileKotlinJs")

tasks {

    register("buildWebgl") {
        group = "paprika"

        dependsOn("compileKotlinJs")

        val scripts = arrayListOf<String>()
        (outputs.files + configurations["jsRuntimeClasspath"]).forEach { file ->
            if (file.isFile)
                copy {
                    includeEmptyDirs = false

                    from(if (file.extension == "js") file else zipTree(file.absolutePath))
                    into("$projectDir/dist/js/")
                    include { fileTreeElement ->
                        val path = fileTreeElement.path
                        if (path.endsWith(".js") && !path.endsWith("meta.js"))
                            scripts.add("js/$path")
                        (path.endsWith(".js") || path.endsWith(".map")) && (path.startsWith("META-INF/resources/") || !path.startsWith("META-INF/"))
                    }
                }
        }

        copy {
            from("src/jsMain/resources/index.html")
            into("dist/")
            filter {
                it.replace("@@script@@", scripts.reversed().joinToString("") { "\n\t<script src=\"$it\"></script>" })
                    .replace("@@app-script@@", "js/${project.name}.js")
            }
        }

        copy {
            from("../assets")
            into("dist/assets")
        }
    }

    register<JavaExec>("runLwjgl") {
        jvmArgs("-XstartOnFirstThread")
        dependsOn("compileKotlinJvm")

        group = "paprika"
        main = "ca.jsbr.tappyPlane.MainKt"
        val target = kotlin.targets["jvm"]
        val compilation = target.compilations["main"] as org.jetbrains.kotlin.gradle.plugin.mpp.KotlinJvmCompilation
        val classes = files(
            compilation.runtimeDependencyFiles,
            compilation.output.allOutputs
        )
        classpath = classes
    }

    register("copyFramework") {
        group = "paprika"
        val buildType = project.findProperty("kotlin.build.type") as? String ?: "DEBUG"
        val target = project.findProperty("kotlin.target") as? String ?: "ios"

        val targetBin = (kotlin.targets[target] as org.jetbrains.kotlin.gradle.plugin.mpp.KotlinNativeTarget).binaries
        dependsOn(targetBin.getFramework(buildType).linkTask)

        doLast {
            val srcFile = targetBin.getFramework(buildType).outputFile
            val targetDir = project.property("configuration.build.dir")
            copy {
                from(srcFile.parent)
                into(targetDir as String)
                include("app.framework/**")
                include("app.framework.dSYM")
            }
        }
    }
}

