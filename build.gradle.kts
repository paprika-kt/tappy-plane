buildscript {

    repositories {
        jcenter()
        mavenCentral()
        google()
    }

    dependencies {
        classpath("com.android.tools.build:gradle:3.5.3")
    }
}

plugins {
    kotlin("multiplatform") version "1.3.61" apply false
}
